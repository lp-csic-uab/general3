# -*- coding: utf-8 -*-
"""
:synopsis: Common Basic General Functions for Python 3.x.

:created:    2013/07/05

:authors:    Óscar (ogallardo@protonmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2014 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat
"""

__VERSION__ = '0.3'
__UPDATED__ = '2019-09-17'


#===============================================================================
# Imports
#===============================================================================
import csv
import types
from collections import defaultdict
from functools import reduce

#===============================================================================
# Functions declarations
#===============================================================================
def chunker(seq, size):
    """
    :return: a generator that will slice a sequence (`seq`) in chunks of the
    specified `size`
    """
    return ( seq[pos:pos+size] for pos in range(0, len(seq), size) )


def combine(groups):
    """
    :CAUTION: it's better to use :class:``itertools.product`` instead of this
    function, to avoid excessive memory consumption and speed-up.
    
    Generates all the combinations between the elements/objects of the
    different groups.
    [ [1,2], [3], [4,5] ] -> [ (1,3,4), (1,3,5), (2,3,4), (2,3,5) ]
    
    :param iterable groups: an iterable containing groups of elements/objects.
    
    :return list : a list containing all the combinations between the
    elements/objects as tuples.
    """
    # [ (,) ] -> [ (1,), (2,) ] -> [ (1,3), (2,3) ] -> [ (1,3,4), (1,3,5), (2,3,4), (2,3,5) ]
    combinations = [tuple()]
    new_combinations = list()
    for group in groups:
        for elem in group:
            for comb in combinations:
                new_combinations.append( comb + (elem,) )
        combinations = new_combinations #
        new_combinations = list()
    return combinations


def flatten_1lvl(iterable_of_lists_or_tuples):
    """
    Flatten an iterable of objects that support :method:``__add__`` (lists,
    tuples, ... but also strings, integers, ...).
    """
    return reduce(lambda x, y: x + y, iterable_of_lists_or_tuples)


def deep_getattr(obj, deep_attr, *args):
    """
    :return: the value of the last attribute in a chain of attributes. Ex.:
      deep_getattr(obj, 'x.y.z') will return obj.x.y.z
    """
    not_set = object()
    default = not_set
    if args and len(args) == 1:
        default = args[0]
    elif len(args) > 1:
        raise TypeError('deep_getattr expected at most 3 arguments')
    chained_attrs = deep_attr.split('.')
    value = obj
    for attr in chained_attrs:
        if default is not not_set:
            value = getattr(value, attr, default)
        else:
            value = getattr(value, attr)
    return value

def group_by_attrs(objects, attr, *attrs):
    """
    Group `objects` (iterable of instances) by one (`attr`) or more (`attrs`)
    of its attributes/properties values. Those attribute values should be
    hashable.
    """
    grouped = defaultdict(list)
    if attrs:
        attrs = (attr,) + attrs
    # Iterate over objects and group by gid (group ID) in a dictionary of sets:
    for obj in objects:
        if not attrs:
            gid = deep_getattr(obj, attr)
        else:
            multiple_gid = list()
            for an_attr in attrs:
                multiple_gid.append(deep_getattr(obj, an_attr))
            gid = tuple(multiple_gid)
        grouped[gid].append(obj)
    #
    return grouped


def try_func_except_default(func, default=None, except_cls=Exception):
    def wrapped_func(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except except_cls as _:
            return default(*args, **kwargs) if callable(default) else default
    return wrapped_func


def str_translate(string, mapping_table=None, other_char_tmplt='{0}'):
    """
    Formating of original string according to a `mapping_table` dictionary. 
    Ex. with mapping_table={'e': 'eee', 'o': 'O'}: Hello -> HeeellO
    
    :param string string: and aminoacid sequence.
    :param dict mapping_table: a dictionary that maps characters in `string` to
    other characters or strings. Ex.: { char: string }. Defaults to None.
    :param string other_char_tmplt: a template to apply to characters not in the 
    `mapping_table` dictionary. Defaults to '{0}' (let the char without change).
    
    :return string : a new sequence from the original one according to the
    `mapping_table` replacements.
    """
    if mapping_table:
        char_map_keys = mapping_table.keys()
        return "".join( mapping_table[char] if char in char_map_keys 
                        else other_char_tmplt.format(char) for char in string )
    else:
        return "".join( other_char_tmplt.format(char) for char in string )


def iter_strs_with_strs_in(query_strs, target_strs):
    """
    Yields string of `target_strs` containing any of the strings from `query_strs`.

    :param iterable query_strs: iterable of strings to search for.
    :param iterable target_strs: iterable of strings to search in.
    
    :return generator : yields Each string of `target_strs` Containing Any
    string from `query_strs`. Ex.: 
        ('a', 'r') in ('spam', 'circus', 'python') -> 'spam', 'circus'
    """
    for t_str in target_strs:
        for q_str in query_strs:
            if q_str in t_str:
                yield t_str
                break

def any_strs_in_strs(query_strs, target_strs):
    """
    Fast check for any string of `query_strs` contained in any string from
    `target_strs`.

    :param iterable query_strs: iterable of strings to search for.
    :param iterable target_strs: iterable of strings to search in.
    
    :return bool : True if Any string from `query_strs` is Contained in Any
    string from `target_strs`, False otherwise; or None in Special cases. Ex.: 
        ('a', 'r') in ('spam', 'circus', 'python') -> True
    """
    if not target_strs or set(target_strs) == set( [''] ): # Special cases: empty :param:`target_strs`:
        return None
    for _ in iter_strs_with_strs_in(query_strs, target_strs): # Get some coincidence:
        return True # Return true at first coincidence found (and then stop iterating).
    return False # No coincidence.


def iter_strs_within_strs(query_strs, target_strs):
    """
    Yields string of `query_strs` contained in any of the strings from 
    `target_strs`.

    :param iterable query_strs: iterable of strings to search for.
    :param iterable target_strs: iterable of strings to search in.
    
    :return generator : yields Each string from `query_strs` Contained In Any
    string from `target_strs`. Ex.: 
        ('a', 'r', 'q') in ('spam', 'circus') -> 'a', 'r'
    """
    for q_str in query_strs:
        for t_str in target_strs:
            if q_str in t_str:
                yield q_str
                break


def save_iterable(file_name, str_iterable, file_mode='wt'):
    """
    Save an iterable of strings (`str_iterable`) to file `file_name`
    """
    with open(file_name, file_mode) as iofile:
        iofile.write( '\n'.join(str_iterable) )


def save_dicts_as_csv(output_file, dicts_iterable, file_mode='wt',
                      fieldnames=None, restval='', extrasaction='ignore',
                      delimiter=',', *args, **kwargs):
    """
    Save an iterable of dictionaries (`dicts_iterable`) to file `output_file`.
    
    :param object output_file: a filename string or a file-like object.
    :param iterable dicts_iterable: an iterable of dictionary (dict-like) objects.
    :param str file_mode: the file-mode to create a new output file if 
    `output_file` is a file name; otherwise not used. Defaults to 'wb'.
    """
    # Check mandatory :param:`output_file`:
    if isinstance(output_file, str): #It is a filename string -> create a :class:``file`` object:
        iofile = open(output_file, file_mode)
        close_iofile = True
    elif hasattr(output_file, 'write'): #It is a file-like object:
        iofile = output_file
        close_iofile = False
    else:
        raise TypeError("First argument should be a filename string or a file-like object.")
    # Ensure :param:`dicts_iterable` is an iterator-like to treat it in a
    # homogeneous way:
    iter_rowdicts = iter(dicts_iterable)
    # Check needed :param:`fieldnames`:
    first_rowdict = None
    if fieldnames is None: #Try to get it from :param:`dicts_iterable` keys.
        try:
            first_rowdict = iter_rowdicts.next()
            fieldnames = first_rowdict.keys()
        except (StopIteration, AttributeError) as _: # No data in iterable :param:`dicts_iterable` or it is not dict-like:
            pass
    if fieldnames is not None: # We have all the needed parameters for :class.:``csv.DictWriter``:        
        csvdw = csv.DictWriter(iofile, fieldnames, restval=restval, #Create the :class:``csv.DictWriter`` instance.
                               extrasaction=extrasaction, delimiter=delimiter,
                               *args, **kwargs)
        # Write data to the file-like object:
        csvdw.writeheader() #First the header field names
        if first_rowdict: #Then the first row if it has been used to determine field names:
            csvdw.writerow(first_rowdict)
        for rowdict in iter_rowdicts: #Write the remaining data:
            csvdw.writerow(rowdict) #X-NOTE: :method:``DictWriter.writerows`` not used because it generates an intermediate list of lists that can consume a lot of memory
    else: # :param:`dict_iterable` contains no data, or no valid data (dict-like objects):
        iofile.write('No data!\n')
    # Close the file object, if it has been previously opened within this
    # function context:
    if close_iofile:
        iofile.close()


def copy_func(func, name=None, fglobals=None, module=None, fdict=None):
    """
    Copy a function object.
    X_NOTE: Based on https://stackoverflow.com/a/13503277
    """
    cfunc = types.FunctionType(func.__code__, fglobals or func.__globals__, 
                               name=name or func.__name__,
                               argdefs=func.__defaults__,
                               closure=func.__closure__)
#    cfunc = functools.update_wrapper(g, f)
    cfunc.__doc__ = func.__doc__
    cfunc.__module__ = module or func.__module__
    cfunc.__dict__.update(fdict or func.__dict__)
    return cfunc

