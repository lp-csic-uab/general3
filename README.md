---

**WARNING!**: This is the *Old* source-code repository for General3 shared Package from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/sharedpackages/general3_code/) located at https://sourceforge.net/p/lp-csic-uab/sharedpackages/general3_code/**  

---  
  
  
# General3 shared Package

General Python 3.x code to reuse in different projects.


---

**WARNING!**: This is the *Old* source-code repository for General3 shared Package from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/sharedpackages/general3_code/) located at https://sourceforge.net/p/lp-csic-uab/sharedpackages/general3_code/**  

---  
  